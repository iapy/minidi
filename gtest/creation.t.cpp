#include <gtest/gtest.h>

#include <minidi_framework.h>

namespace {

class A {};
class B {};

class Container
: public minidi::Component<A>
, public minidi::Component<B>
{
public:
    typedef minidi::Component<A> a;
    typedef minidi::Component<B> b;

    Container(minidi::Bind const &binder = minidi::Bind())
    : minidi::Container(binder)
    {}
};

GTEST_TEST(Creation, default_binder)
{
    Container container;

    GTEST_ASSERT_NE(container.a::ptr(), nullptr);
    GTEST_ASSERT_NE(container.b::ptr(), nullptr);
}

GTEST_TEST(Creation, null_binder)
{
    minidi::Bind binder(minidi::Bind::DEFAULT_NULL);
    Container container(binder);

    GTEST_ASSERT_EQ(container.a::ptr(), nullptr);
    GTEST_ASSERT_EQ(container.b::ptr(), nullptr);
}

GTEST_TEST(Creation, partial_binder_default_null)
{
    minidi::Bind binder(minidi::Bind::DEFAULT_NULL);
    binder.bind<Container::a>(&Container::a::make);

    Container container(binder);

    GTEST_ASSERT_NE(container.a::ptr(), nullptr);
    GTEST_ASSERT_EQ(container.b::ptr(), nullptr);
};

GTEST_TEST(Creation, partial_binder_default_not_null)
{
    minidi::Bind binder(minidi::Bind::DEFAULT_MAKE);
    binder.bind<Container::b>(&Container::b::null);

    Container container(binder);

    GTEST_ASSERT_NE(container.a::ptr(), nullptr);
    GTEST_ASSERT_EQ(container.b::ptr(), nullptr);
};

}
