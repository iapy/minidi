#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <minidi_framework.h>
#include <vector>

namespace {
class A;
class B;
class C;

class Container
: public minidi::Component< minidi::Abstract<A> >
, public minidi::Component< minidi::Abstract<B> >
, public minidi::Component<C>
{
public:
    typedef minidi::Component< minidi::Abstract<A> > a;
    typedef minidi::Component< minidi::Abstract<B> > b;
    typedef minidi::Component<C> c;
};

class A
{
public:
    virtual void test() = 0;
};

class B {};
class C {};

GTEST_TEST(TestForward, test_forward)
{
    Container container;

    GTEST_ASSERT_EQ(container.a::get(), nullptr);
    GTEST_ASSERT_EQ(container.b::get(), nullptr);
    GTEST_ASSERT_NE(container.c::get(), nullptr);
};

}
