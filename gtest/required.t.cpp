#include <gtest/gtest.h>

#include <minidi_framework.h>

namespace {

class A {};

class B
{
public:
    void a(std::shared_ptr<A> const &value) { a_ = value; }
    std::shared_ptr<A> a() { return a_; }
private:
    std::shared_ptr<A> a_;
};

class C
{
public:
    std::shared_ptr<A> a;
};

class Container
: public minidi::Component<A>
, public minidi::Component<B>
, public minidi::Component<C>
{
public:
    typedef minidi::Component<A> a;
    typedef minidi::Component<B> b;
    typedef minidi::Component<C> c;

    Container(minidi::Bind const &binder = minidi::Bind())
    : minidi::Container(binder)
    {
        b::requires<a>(&B::a);
        c::requires<a>(&C::a);
    }
};

GTEST_TEST(Required, default_binder)
{
    Container container;

    GTEST_ASSERT_NE(container.a::ptr(), nullptr);
    GTEST_ASSERT_NE(container.b::ptr(), nullptr);
    GTEST_ASSERT_NE(container.c::ptr(), nullptr);
    GTEST_ASSERT_EQ(container.b::ptr()->a(), container.a::ptr());
    GTEST_ASSERT_EQ(container.c::ptr()->a,   container.a::ptr());
}

GTEST_TEST(Required, null_binder)
{
    minidi::Bind binder(minidi::Bind::DEFAULT_NULL);
    Container container(binder);

    GTEST_ASSERT_EQ(container.a::ptr(), nullptr);
    GTEST_ASSERT_EQ(container.b::ptr(), nullptr);
    GTEST_ASSERT_EQ(container.c::ptr(), nullptr);
}

GTEST_TEST(Required, ownee_is_null_setter)
{
    minidi::Bind binder(minidi::Bind::DEFAULT_MAKE);
    binder.bind<Container::a>(&Container::a::null);
    binder.bind<Container::c>(&Container::c::null);

    ASSERT_THROW({
        Container container(binder);
    }, minidi::Exception);
}

GTEST_TEST(Required, ownee_is_null_property)
{
    minidi::Bind binder(minidi::Bind::DEFAULT_MAKE);
    binder.bind<Container::a>(&Container::a::null);
    binder.bind<Container::b>(&Container::b::null);

    ASSERT_THROW({
        Container container(binder);
    }, minidi::Exception);
}

GTEST_TEST(Required, owner_is_null)
{
    minidi::Bind binder(minidi::Bind::DEFAULT_MAKE);
    binder.bind<Container::b>(&Container::b::null);

    Container container(binder);

    GTEST_ASSERT_NE(container.a::ptr(), nullptr);
    GTEST_ASSERT_EQ(container.b::ptr(), nullptr);
}

}
