#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <minidi_framework.h>
#include <vector>

namespace {
class Observer
{
public:
    virtual void notify(int event) = 0;
};

class Observant
{
public:
    void addObserver(std::shared_ptr<Observer> const &observer) { d_observers.push_back(observer); }

    void notifyAll(int event)
    {
        for(std::vector< std::shared_ptr<Observer> >::const_iterator it = d_observers.begin();
            it != d_observers.end(); ++it)
        {
            (*it)->notify(event);
        }
    }
private:
    std::vector< std::shared_ptr<Observer> > d_observers;
};

class MockObserver: public Observer
{
public:
    MOCK_METHOD1(notify, void(int event));
};

class Container
: public minidi::Component<Observant>
, public minidi::Component<Observer, 1>
, public minidi::Component<Observer, 2>
{
public:
    typedef minidi::Component<Observant> observant;
    typedef minidi::Component<Observer, 1> observer1;
    typedef minidi::Component<Observer, 2> observer2;

    Container(minidi::Bind const &binder = minidi::Bind())
    : minidi::Container(binder)
    {
        observant::optional<observer1>(&Observant::addObserver);
        observant::optional<observer2>(&Observant::addObserver);
    }
};

GTEST_TEST(Abstract, both_nulls)
{
    Container container;

    GTEST_ASSERT_NE(container.observant::ptr(), nullptr);
    GTEST_ASSERT_EQ(container.observer1::ptr(), nullptr);
    GTEST_ASSERT_EQ(container.observer1::ptr(), nullptr);

    // should not crash
    container.observant::get()->notifyAll(1);
}

GTEST_TEST(Abstract, create_mock)
{
    minidi::Bind binder;
    binder.bind<Container::observer1>(&Container::observer1::subclass<MockObserver>);

    Container container(binder);

    GTEST_ASSERT_NE(container.observant::ptr(), nullptr);
    GTEST_ASSERT_NE(container.observer1::ptr(), nullptr);
    GTEST_ASSERT_EQ(container.observer2::ptr(), nullptr);

    MockObserver *observer1 = dynamic_cast<MockObserver*>(container.observer1::get());
    EXPECT_CALL(*observer1, notify(1)).Times(1);

    container.observant::get()->notifyAll(1);
}

GTEST_TEST(Abstract, create_mocks)
{
    minidi::Bind binder;
    binder.bind<Container::observer1>(&Container::observer1::subclass<MockObserver>);
    binder.bind<Container::observer2>(&Container::observer2::subclass<MockObserver>);

    Container container(binder);

    GTEST_ASSERT_NE(container.observant::ptr(), nullptr);
    GTEST_ASSERT_NE(container.observer1::ptr(), nullptr);
    GTEST_ASSERT_NE(container.observer2::ptr(), nullptr);

    MockObserver *observer1 = dynamic_cast<MockObserver*>(container.observer1::get());
    EXPECT_CALL(*observer1, notify(1)).Times(1);

    MockObserver *observer2 = dynamic_cast<MockObserver*>(container.observer2::get());
    EXPECT_CALL(*observer2, notify(1)).Times(1);

    container.observant::get()->notifyAll(1);
}
}
