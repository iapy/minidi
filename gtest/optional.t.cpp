#include <gtest/gtest.h>

#include <minidi_framework.h>

namespace {

class A {};

class B
{
public:
    void a(std::shared_ptr<A> const &value) { a_ = value; }
    std::shared_ptr<A> a() { return a_; }
private:
    std::shared_ptr<A> a_;
};

class C
{
public:
    std::shared_ptr<A> a;
};

class Container
: public minidi::Component<A>
, public minidi::Component<B>
, public minidi::Component<C>
{
public:
    typedef minidi::Component<A> a;
    typedef minidi::Component<B> b;
    typedef minidi::Component<C> c;

    Container(minidi::Bind const &binder = minidi::Bind())
    : minidi::Container(binder)
    {
        b::optional<a>(&B::a);
        c::optional<a>(&C::a);
    }
};

GTEST_TEST(Optional, default_binder)
{
    Container container;

    GTEST_ASSERT_NE(container.a::ptr(), nullptr);
    GTEST_ASSERT_NE(container.b::ptr(), nullptr);
    GTEST_ASSERT_NE(container.c::ptr(), nullptr);
    GTEST_ASSERT_EQ(container.b::ptr()->a(), container.a::ptr());
    GTEST_ASSERT_EQ(container.c::ptr()->a,   container.a::ptr());
}

GTEST_TEST(Optional, null_binder)
{
    minidi::Bind binder(minidi::Bind::DEFAULT_NULL);
    Container container(binder);

    GTEST_ASSERT_EQ(container.a::ptr(), nullptr);
    GTEST_ASSERT_EQ(container.b::ptr(), nullptr);
    GTEST_ASSERT_EQ(container.c::ptr(), nullptr);
}

GTEST_TEST(Optional, ownee_is_null)
{
    minidi::Bind binder(minidi::Bind::DEFAULT_MAKE);
    binder.bind<Container::a>(&Container::a::null);

    Container container(binder);

    GTEST_ASSERT_EQ(container.a::ptr(), nullptr);
    GTEST_ASSERT_NE(container.b::ptr(), nullptr);
    GTEST_ASSERT_NE(container.c::ptr(), nullptr);
    GTEST_ASSERT_EQ(container.b::ptr()->a(), nullptr);
    GTEST_ASSERT_EQ(container.c::ptr()->a,   nullptr);
}

GTEST_TEST(Optional, owner_is_null)
{
    minidi::Bind binder(minidi::Bind::DEFAULT_MAKE);
    binder.bind<Container::b>(&Container::b::null);
    binder.bind<Container::c>(&Container::c::null);

    Container container(binder);

    GTEST_ASSERT_NE(container.a::ptr(), nullptr);
    GTEST_ASSERT_EQ(container.b::ptr(), nullptr);
    GTEST_ASSERT_EQ(container.c::ptr(), nullptr);
}

}
