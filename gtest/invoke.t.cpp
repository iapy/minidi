#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <minidi_framework.h>

namespace {

class A
{
public:
    virtual void test() = 0;
    virtual void test(int a) = 0;
    virtual void test(int a, int const &b) = 0;
};

class MockA: public A
{
public:
    MOCK_METHOD0(test, void());
    MOCK_METHOD1(test, void(int));
    MOCK_METHOD2(test, void(int, int const &));
};

class Container
: public minidi::Component<A>
{
public:
    typedef minidi::Component<A> a;

    Container(minidi::Bind const &binder = minidi::Bind())
    : minidi::Container(binder)
    {}

    void test()
    {
        const int two = 2;

        a::invoke(&A::test);
        a::invoke(&A::test, 1);
        a::invoke(&A::test, 1, two);
    }
};

GTEST_TEST(Invoke, null)
{
    Container container;
    ASSERT_EQ(container.a::ptr(), nullptr);

    // should not crash
    container.test();
}

GTEST_TEST(Invoke, mocked)
{
    minidi::Bind binder;
    binder.bind<Container::a>(&Container::a::subclass<MockA>);

    Container container(binder);

    ASSERT_NE(container.a::ptr(), nullptr);

    MockA &mocka = *dynamic_cast<MockA*>(container.a::get());
    EXPECT_CALL(mocka, test()).Times(1);
    EXPECT_CALL(mocka, test(1)).Times(1);
    EXPECT_CALL(mocka, test(1,2)).Times(1);

    container.test();
}

}
