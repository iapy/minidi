#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <minidi_framework.h>

namespace {
class A{};

class Integer
{
public:
    Integer(const int &value)
    : d_value(value)
    {}

    const int value() const { return d_value; }

private:
    const int d_value;
};

class Container
: public minidi::Component<A>
, public minidi::Component<Integer>
{
public:
    typedef minidi::Component<A> a;
    typedef minidi::Component<Integer> integer;

    Container(int value, minidi::Bind const &binder = minidi::Bind())
    : minidi::Container(binder)
    , integer(value)
    {}

    static std::shared_ptr<Integer> make_integer(int const &value)
    {
        return std::shared_ptr<Integer>(new Integer(value + 1));
    }
};

GTEST_TEST(Argument, simple)
{
    Container container(42);

    ASSERT_NE(container.a::ptr(), nullptr);
    ASSERT_NE(container.integer::ptr(), nullptr);
    ASSERT_EQ(container.integer::get()->value(), 42);
}

GTEST_TEST(Argument, with_null)
{
    minidi::Bind binder(minidi::Bind::DEFAULT_NULL);

    Container container(42, binder);

    ASSERT_EQ(container.a::ptr(), nullptr);
    ASSERT_EQ(container.integer::ptr(), nullptr);
}

GTEST_TEST(Argument, with_binder)
{
    minidi::Bind binder(minidi::Bind::DEFAULT_NULL);
    binder.bind<Container::integer>(&Container::make_integer);

    Container container(42, binder);

    ASSERT_EQ(container.a::ptr(), nullptr);
    ASSERT_NE(container.integer::ptr(), nullptr);
    ASSERT_EQ(container.integer::ptr()->value(), 43);
}
}
