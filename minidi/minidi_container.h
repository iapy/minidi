#ifndef INCLUDED_MINIDI_CONTAINER_H
#define INCLUDED_MINIDI_CONTAINER_H

#include <map>

namespace minidi {
namespace detail {

template<class T, unsigned ID>
struct Slot
{
    typedef T type;
    static int key;
};

} // namespace detail

class Bind
{
private:
    typedef std::map<int *, void *> InitializersMap;

public:
    enum Mode
    {
        DEFAULT_NULL,
        DEFAULT_MAKE
    };

    Bind(const Mode mode = DEFAULT_MAKE)
    : mode_(mode)
    {}

    template<class COMPONENT, class ... ARGS>
    void bind(std::shared_ptr<typename COMPONENT::slot::type> (*initializer)(ARGS const & ...));

private:
    Mode mode_;
    InitializersMap initializers_;

    friend class Container;
};

class Container
{
public:
    Container()
    : mode_(Bind::DEFAULT_MAKE)
    , initializers_(nullptr)
    {}

    Container(Bind const &binder)
    : mode_(binder.mode_)
    , initializers_(&binder.initializers_)
    {}

protected:
    virtual ~Container() {}

    template<class COMPONENT, typename ... ARGS>
    std::shared_ptr<typename COMPONENT::slot::type> create(ARGS && ...) const;

private:
    Bind::Mode mode_;
    Bind::InitializersMap const *initializers_;
};

template<class T, unsigned ID = 0>
class Component
: protected virtual Container
, public detail::Component< typename detail::RealType<T>::type, detail::RealType<T>::is_abstract >
{
private:
    typedef detail::RealType<T> RealType;
    typedef detail::Component< typename RealType::type, RealType::is_abstract > Base;

public:
    typedef detail::Slot<typename RealType::type, ID> slot;

    template<typename ... ARGS>
    Component(ARGS && ... args)
    : Base(Container::create< Component<T,ID> >(args ...)) {}

protected: // dependency injection
    template<class COMPONENT> void optional(void (T::*setter)(std::shared_ptr<typename COMPONENT::slot::type> const &));
    template<class COMPONENT> void requires(void (T::*setter)(std::shared_ptr<typename COMPONENT::slot::type> const &));

    template<class COMPONENT> void optional(std::shared_ptr<typename COMPONENT::slot::type> (T::*property));
    template<class COMPONENT> void requires(std::shared_ptr<typename COMPONENT::slot::type> (T::*property));

private:
    using Base::d_instance;
    template<class, unsigned> friend class Component;
};

////////////////////////////////////
// class detail::Slot implementation
////////////////////////////////////

template<class T, unsigned ID> int detail::Slot<T, ID>::key = 42;

////////////////////////////////////
// class detail::Bind implementation
////////////////////////////////////

template<class COMPONENT, class ... ARGS>
void Bind::bind(std::shared_ptr<typename COMPONENT::slot::type> (*initializer)(ARGS const & ...))
{
    initializers_[&COMPONENT::slot::key] = reinterpret_cast<void*>(initializer);
}

/////////////////////////////////
// class Container implementation
/////////////////////////////////

template<class COMPONENT, class ... ARGS>
std::shared_ptr<typename COMPONENT::slot::type> Container::create(ARGS && ... args) const
{
    typedef typename detail::Initializer<typename COMPONENT::slot::type>::template with_args<ARGS ...>::type Initialiser;

    if(initializers_)
    {
        Bind::InitializersMap::const_iterator p = initializers_->find(&COMPONENT::slot::key);
        if(p != initializers_->end())
        {
            return reinterpret_cast<Initialiser>(p->second)(std::forward<ARGS>(args) ...);
        }
    }
    if(mode_ == Bind::DEFAULT_NULL)
    {
        return COMPONENT::null();
    }
    else
    {
        return COMPONENT::make(std::forward<ARGS>(args) ...);
    }
}

/////////////////////////////////
// class Component implementation
/////////////////////////////////

template<class T, unsigned ID>
template<class COMPONENT>
void Component<T, ID>::optional(void (T::*setter)(std::shared_ptr<typename COMPONENT::slot::type> const &))
{
    COMPONENT *component = dynamic_cast<COMPONENT*>(this);
    assert(component != NULL); // the component is from the same container

    if(d_instance && component->d_instance)
    {
        ((d_instance.get())->*setter)(component->d_instance);
    }
}

template<class T, unsigned ID>
template<class COMPONENT>
void Component<T, ID>::requires(void (T::*setter)(std::shared_ptr<typename COMPONENT::slot::type> const &))
{
    COMPONENT *component = dynamic_cast<COMPONENT*>(this);
    assert(component != NULL); // the component is from the same container

    if(d_instance)
    {
        if(component->d_instance)
        {
            ((d_instance.get())->*setter)(component->d_instance);
        }
        else
        {
            throw Exception(Exception::REQUIRED_COMPONENT_IS_NOT_INITIALIZED);
        }
    }
}

template<class T, unsigned ID>
template<class COMPONENT>
void Component<T, ID>::optional(std::shared_ptr<typename COMPONENT::slot::type> (T::*property))
{
    COMPONENT *component = dynamic_cast<COMPONENT*>(this);
    assert(component != NULL); // the component is from the same container

    if(d_instance && component->d_instance)
    {
        ((d_instance.get())->*property) = component->d_instance;
    }
}

template<class T, unsigned ID>
template<class COMPONENT>
void Component<T, ID>::requires(std::shared_ptr<typename COMPONENT::slot::type> (T::*property))
{
    COMPONENT *component = dynamic_cast<COMPONENT*>(this);
    assert(component != NULL); // the component is from the same container

    if(d_instance)
    {
        if(component->d_instance)
        {
            ((d_instance.get())->*property) = component->d_instance;
        }
        else
        {
            throw Exception(Exception::REQUIRED_COMPONENT_IS_NOT_INITIALIZED);
        }
    }
}

} // namespace minidi

#endif
