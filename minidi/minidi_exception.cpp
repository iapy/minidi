#include <minidi_exception.h>

#include <memory>

namespace minidi {

const char *Exception::what() const throw()
{
    switch (d_reason) {
        case REQUIRED_COMPONENT_IS_NOT_INITIALIZED:
            return "Required component is not initialized";
    }
    return nullptr;
}

}
