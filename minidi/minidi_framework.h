#ifndef INCLUDED_MINIDI_FRAMEWORK_H
#define INCLUDED_MINIDI_FRAMEWORK_H

#include <minidi_exception.h>
#include <minidi_component.h>
#include <minidi_real_type.h>
#include <minidi_container.h>

#endif
