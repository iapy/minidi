#ifndef INCLUDED_MINIDI_EXCEPTION_H
#define INCLUDED_MINIDI_EXCEPTION_H

#include <exception>

namespace minidi {

class Exception: public std::exception
{
public:
    enum Reason
    {
        REQUIRED_COMPONENT_IS_NOT_INITIALIZED
    };

    Exception(Reason const &reason)
    : d_reason(reason)
    {}

    const char * what() const throw();
    
private:
    Reason d_reason;
};

} // namespace minidi

#endif
