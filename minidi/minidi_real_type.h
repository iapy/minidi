#ifndef INCLUDED_MINIDI_REAL_TYPE_H
#define INCLUDED_MINIDI_REAL_TYPE_H

namespace minidi {
template<class T>
struct Abstract
{
    typedef T type;
};

namespace detail {

/**
 * This implementation of std::is_abstract was taken from clang's stdc++.
 * IsAbstract always compiles and its value is 0 for undefined types
 */
template<class T>
class IsAbstract
{
    typedef char _yes[2];

    template <class C> static char &__test(C (*)[1]);
    template <class C> static _yes &__test(...);
public:
    enum { value = (sizeof(__test<T>(0)) == sizeof(_yes)) };
};

template<class T>
struct RealType
{
    typedef T type;
    enum { is_abstract = IsAbstract<T>::value };
};

template<class T>
struct RealType< Abstract<T> >
{
    typedef typename Abstract<T>::type type;
    enum { is_abstract = 1 };
};

} // namespace detail
} // namespace minidi

#endif
