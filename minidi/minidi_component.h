#ifndef INCLUDED_MINIDI_COMPONENT_H
#define INCLUDED_MINIDI_COMPONENT_H

#include <memory>
#include <type_traits>

namespace minidi {
namespace detail {

template<class T>
struct Initializer
{
    typedef std::shared_ptr<T> (*type)();

    template<typename ... ARGS>
    struct with_args
    {
        typedef std::shared_ptr<T> (*type)(ARGS const & ...);
    };
};

template<class T, bool ABSTRACT>
struct DefaultInitializer
{
    inline static std::shared_ptr<T> make()
    {
        return std::shared_ptr<T>();
    }
};

template<class T>
struct DefaultInitializer<T, false>
{
    inline static std::shared_ptr<T> make()
    {
        return std::shared_ptr<T>(new T);
    }

    template<class ... ARGS>
    inline static std::shared_ptr<T> make(ARGS const & ... args)
    {
        return std::shared_ptr<T>(new T(args ...));
    }
};

template<class T, bool ABSTRACT>
class Component
: public DefaultInitializer<T, ABSTRACT>
{
protected:
    Component(std::shared_ptr<T> const &instance)
    : d_instance(instance) {}

public: // getters
    inline T* get();
    inline std::shared_ptr<T> ptr();

public: // initializers
    inline static std::shared_ptr<T> null();

    template<class S> inline static std::shared_ptr<T> subclass();

public: // invokers
    template<typename ... ARGS>
    void invoke(void (T::*method)(ARGS ...), ARGS && ... args);

protected:
    std::shared_ptr<T> d_instance;
};

/////////////////////////////////
// class Component implementation
/////////////////////////////////

template<class T, bool ABSTRACT>
T * Component<T, ABSTRACT>::get()
{
    return d_instance.get();
}

template<class T, bool ABSTRACT>
std::shared_ptr<T> Component<T, ABSTRACT>::ptr()
{
    return d_instance;
}

template<class T, bool ABSTRACT>
std::shared_ptr<T> Component<T, ABSTRACT>::null()
{
    return std::shared_ptr<T>();
}

template<class T, bool ABSTRACT>
template<class S>
std::shared_ptr<T> Component<T, ABSTRACT>::subclass()
{
    return std::shared_ptr<T>(new S);
}

template<class T, bool ABSTRACT>
template<typename ... ARGS>
void Component<T, ABSTRACT>::invoke(void (T::*method)(ARGS ...), ARGS && ... args)
{
    if(d_instance)
    {
        ((d_instance.get())->*method)(std::forward<ARGS>(args) ...);
    }
}

} // namespace detail
} // namespace minidi

#endif
